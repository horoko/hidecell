//
//  RandomNumberHistoryCell.swift
//  GetRandom
//
//  Created by admin on 13.09.2017.
//  Copyright © 2017 horoko. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    override var isHidden: Bool {
        get {
            return super.isHidden
        }
        set(v) {
            super.isHidden = v
        }
    }
    
    override var alpha: CGFloat {
        get {
            return super.alpha
        }
        set(v) {
            super.alpha = v
        }
    }
}

