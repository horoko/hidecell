//
//  RandomNumberScrollController.swift
//  GetRandom
//
//  Created by admin on 13.09.2017.
//  Copyright © 2017 horoko. All rights reserved.
//

import UIKit

class ScrollController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let height = UIScreen.main.bounds.size.height - UIApplication.shared.statusBarFrame.height
        scrollView.contentSize = CGSize(width: view.frame.size.width * 2, height: height)
        scrollView.isPagingEnabled = true
        
        let mainVC = storyboard?.instantiateViewController(withIdentifier: "FirstVC")
        mainVC?.view.backgroundColor = .lightGray
        let historyVC = storyboard?.instantiateViewController(withIdentifier: "RandomNumberHistoryController")
        historyVC?.view.backgroundColor = .lightGray
        scrollView.addSubview(mainVC!.view)
        scrollView.addSubview(historyVC!.view)
        mainVC?.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: height)
        historyVC?.view.frame = CGRect(x: view.frame.size.width, y: 0, width: view.frame.size.width, height: height)
    }

}

