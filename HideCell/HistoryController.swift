import UIKit

class HistoryController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension HistoryController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryCell
        cell.label.text = "\(indexPath.row). \(indexPath.row + 3)"
        return cell
    }
    
}

